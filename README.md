## Homework/Exam Latex Template using Exam Class
The latex template I used for my TA jobs.

This template has been used here as real samples: 
* [Introduction to Mathematics for Communications: Convex Analysis and Optimization - Spring 2019](https://www.ee.nthu.edu.tw/cychi/teaching/Introduction-to-Mathematics-for-Communications-Convex.php)
* [ Convex Optimization for Communications and Signal Processing - Fall 2019](https://www.ee.nthu.edu.tw/cychi/teaching/cvx_comm.php)
