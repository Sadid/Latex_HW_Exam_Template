#!/bin/zsh

name="hw1"

latexmk -pdf -pdflatex='pdflatex -file-line-error' "$name".tex
# latexmk -pdf -pdflatex='pdflatex -file-line-error -interaction nonstopmode' "$name".tex
# rm -f "$name".{out,fdb_latexmk,log,fls,aux,synctex.gz,bbl,bcf,blg,run.xml}

# Create HW with Sol and compile it as well
hwSol="${name}_withSol"
cp "${name}.tex" "${hwSol}.tex"
sed -i 's/% \\printanswers/\\printanswers/' "${hwSol}.tex"
latexmk -pdf -pdflatex='pdflatex -file-line-error -interaction nonstopmode' "${hwSol}.tex"
# rm -f "$hwSol".{tex,out,fdb_latexmk,log,fls,aux,synctex.gz,bbl,bcf,blg,run.xml}

# MyAutoGit
